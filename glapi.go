package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"sort"
	"strings"
	"time"
)

type Author struct {
	Id        int    `json:"id"`
	Username  string `json:"username"`
	Name      string `json:"name"`
	State     string `json:"state"`
	Locked    bool   `json:"locked"`
	AvatarUrl string `json:"avatar_url"`
	WebUrl    string `json:"web_url"`
}

type PushData struct {
	CommitCount int    `json:"commit_count"`
	Action      string `json:"action"`
	RefType     string `json:"ref_type"`
	CommitFrom  string `json:"commit_from"`
	CommitTo    string `json:"commit_to"`
	Ref         string `json:"ref"`
	CommitTitle string `json:"commit_title"`
	RefCount    int    `json:"ref_count"`
}

type Event struct {
	Id             int       `json:"id"`
	ProjectId      int       `json:"project_id"`
	ActionName     string    `json:"action_name"`
	TargetId       int       `json:"target_id"`
	TargetIid      int       `json:"target_iid"`
	TargetType     string    `json:"target_type"`
	AuthorId       int       `json:"author_id"`
	TargetTitle    string    `json:"target_title"`
	CreatedAt      time.Time `json:"created_at"`
	Author         Author    `json:"author"`
	Imported       bool      `json:"imported"`
	ImportedFrom   string    `json:"imported_from"`
	PushData       PushData  `json:"push_data"`
	AuthorUsername string    `json:"author_username"`
}

func getData() []Event {
	log.Println("Requesting data from GitLab")

	apikeyRaw, err := os.ReadFile("token.txt")
	if err != nil {
		log.Fatal(err)
	}
	apikey := strings.TrimSuffix(string(apikeyRaw), "\n")

	// TODO get last 3 years
	after := time.Date(2024, time.January, 1, 0, 0, 0, 0, time.UTC).Format(time.DateOnly)

	client := &http.Client{Timeout: 10 * time.Second}

	user := "eidoom"
	action := "pushed"
	per_page := 100
	page := 1

	var events []Event

	for {
		url := fmt.Sprintf(
			"https://gitlab.com/api/v4/users/%s/events?action=%s&after=%s&per_page=%d&page=%d",
			user,
			action,
			after,
			per_page,
			page)

		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			log.Fatal(err)
		}

		req.Header.Add("PRIVATE-TOKEN", apikey)

		resp, err := client.Do(req)
		if err != nil {
			log.Fatal(err)
		}

		log.Printf("Page %d: response status: %s", page, resp.Status)

		body, err := io.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}

		var result []Event
		err = json.Unmarshal(body, &result)
		if err != nil {
			log.Fatal(err)
		}

		for _, event := range result {
			events = append(events, event)
		}

		if len(result) != per_page {
			break
		}

		page += 1
	}

	return events
}

func getCachedData() []Event {
	var data []Event

	cacheFile := "cache.json"
	_, err := os.Stat(cacheFile)
	if err == nil {
		// file exists
		log.Println("Reading cache file:", cacheFile)

		body, err := os.ReadFile(cacheFile)
		if err != nil {
			log.Fatal(err)
		}

		err = json.Unmarshal(body, &data)
		if err != nil {
			log.Fatal(err)
		}

	} else if errors.Is(err, os.ErrNotExist) {
		// file doesn't exist
		data = getData()

		log.Println("Caching result at:", cacheFile)
		body, err := json.Marshal(data)
		if err != nil {
			log.Fatal(err)
		}

		err = os.WriteFile(cacheFile, body, 0644)
		if err != nil {
			log.Fatal(err)
		}

	} else {
		// error
		log.Fatal(err)
	}

	return data
}

func pushCounter(events []Event) {
	m := make(map[string]int)

	for _, event := range events {
		// this count agrees with the GitLab /users/:id/calendar.json count
		// so it is only counting pushes, not commits
		// I'd rather commits, but how to handle commit_count?
		// it includes other people's commits, for example when merging upstreams into my repos :/
		m[event.CreatedAt.Format(time.DateOnly)] += 1
	}

	keys := make([]string, 0, len(m))

	for k, _ := range m {
		keys = append(keys, k)
	}

	sort.Strings(keys)

	for _, key := range keys {
		fmt.Println(key, m[key])
	}
}

// func indexHandler(w http.ResponseWriter, r *http.Request) {
// 	fmt.Fprintln(w, "TODO")
// }

func main() {
	events := getCachedData()

	pushCounter(events)

	// http.HandleFunc("/", indexHandler)
	// log.Fatal(http.ListenAndServe(":8090", nil))
}
