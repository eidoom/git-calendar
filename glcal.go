package main

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"slices"
	"time"
)

type Bin struct {
	Date  time.Time `json:"date"`
	Count int       `json:"count"`
}

func cmpTime(a, b Bin) int {
	return a.Date.Compare(b.Date)
}

func getData() []Bin {
	log.Println("Requesting data from GitLab")

	client := &http.Client{Timeout: 10 * time.Second}

	req, err := http.NewRequest("GET", "https://gitlab.com/users/eidoom/calendar.json", nil)
	if err != nil {
		log.Fatal(err)
	}

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Response status: %s", resp.Status)

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	// nb map is unordered
	var result map[string]int
	err = json.Unmarshal(body, &result)
	if err != nil {
		log.Fatal(err)
	}

	output := make([]Bin, 0, len(result))
	for k, v := range result {
		date, err := time.Parse("2006-01-02", k)
		if err != nil {
			log.Fatal(err)
		}
		output = append(output, Bin{Date: date, Count: v})
	}

	slices.SortFunc(output, cmpTime)

	return output
}

func dataHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	data := getData()

	b, err := json.Marshal(data)
	if err != nil {
		log.Fatal(err)
	}

	_, err = w.Write(b)
	if err != nil {
		log.Fatal(err)
	}
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "index.html")
}

func main() {
	fs := http.FileServer(http.Dir("static/"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	http.HandleFunc("/data.json", dataHandler)

	http.HandleFunc("/", indexHandler)

	port := "8090"
	log.Printf("Serving at http://localhost:%s\n", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
