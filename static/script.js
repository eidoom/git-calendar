d3.json("/data.json").then((data) => {
  // inspired by https://observablehq.com/@d3/calendar/2
  for (const x of data) {
    x.date = new Date(x.date);
  }

  const [first, last] = d3.extent(data, (x) => x.date);

  d3.difference(
    d3.utcDay.range(first, last),
    data.map((x) => x.date),
  ).forEach((day) => {
    data.push({ date: day, count: 0 });
  });

  const week = d3.utcMonday;
  const days = "MTWTFSS";
  const nWeeks = 1 + week.count(first, last);

  const cellSize = 17;
  const padding = 5;
  const gap = 2;

  const marginLeft = cellSize;
  const marginRight = padding;
  const marginTop = cellSize;
  const marginBottom = padding;

  const width = cellSize * nWeeks + marginLeft + marginRight;
  const height = cellSize * 7 + marginTop + marginBottom;

  const colour = d3
    .scaleSequential(d3.interpolateViridis)
    .domain(d3.extent(data, (d) => d.count));

  // getUTCDay has Sunday <-> 0 but we need to translate so Monday <-> 0
  const getDay = (d) => (d.getUTCDay() + 6) % 7;

  const start = week.floor(first);
  const getWeek = (d) => week.count(start, d);

  const getDate = d3.utcFormat("%d %b");

  const container = d3.select("#d3");

  const svg = container
    .append("svg")
    .attr("width", width)
    .attr("height", height)
    .attr("viewBox", [0, 0, width, height])
    .classed("heatmap", true);

  const chart = svg
    .append("g")
    .attr("transform", `translate(${marginLeft},${marginTop})`);

  // tooltip
  const tooltip = container.append("div").classed("tooltip", true);

  function mouseover() {
    tooltip.style("visibility", "visible");
    d3.select(this).style("stroke", "black");
  }

  function mousemove(e, d) {
    const [x, y] = d3.pointer(e, d);
    console.log(x,y);
    tooltip
      .html(`${getDate(d.date)}: ${d.count}`)
      .style("top", `${y + 25}px`)
      .style("left", `${x}px`);
  }

  function mouseleave() {
    tooltip.style("visibility", "hidden");
    d3.select(this).style("stroke", "none");
  }

  // x-axis label: months
  // discard first month since it's incomplete
  const months = d3.utcMonths(d3.utcMonth(first), last).slice(1);
  chart
    .append("g")
    .attr("text-anchor", "start")
    .attr("dominant-baseline", "bottom")
    .selectAll()
    .data(months)
    .join("text")
    .attr("x", (d) => getWeek(week.ceil(d)) * cellSize + gap)
    .attr("y", -padding)
    .attr("fill", "currentColor")
    .text(d3.utcFormat("%b"));

  // y-axis label: days
  chart
    .append("g")
    .attr("text-anchor", "end")
    .attr("dominant-baseline", "middle")
    .selectAll()
    .data(days)
    .join("text")
    .attr("x", -padding)
    .attr("y", (_, i) => (i + 0.5) * cellSize)
    .attr("fill", "currentColor")
    .text((d) => d);

  // heatmap
  chart
    .append("g")
    .selectAll()
    .data(data)
    .join("rect")
    .attr("width", cellSize - gap)
    .attr("height", cellSize - gap)
    .attr("x", (d) => getWeek(d.date) * cellSize + 0.5)
    .attr("y", (d) => getDay(d.date) * cellSize + 0.5)
    .attr("fill", (d) => (d.count ? colour(d.count) : "transparent"))
    .on("mouseover", mouseover)
    .on("mousemove", mousemove)
    .on("mouseleave", mouseleave);

  // chart
  //   .append("g")
  //   .attr("text-anchor", "middle")
  //   .attr("dominant-baseline", "middle")
  //   .selectAll()
  //   .data(data)
  //   .join("text")
  //   .attr("x", (d) => (getWeek(d.date) + 0.5) * cellSize)
  //   .attr("y", (d) => (getDay(d.date) + 0.5) * cellSize)
  //   .attr("fill", "currentColor")
  //   .text((d) => d.count);
});
