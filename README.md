# git-calendar

I wanted to do a Git calendar for my [projects page](https://eidoom.gitlab.io/projects/).
One can get the data from the GitLab [calendar.json](https://gitlab.com/users/eidoom/calendar.json), or using the [API](https://docs.gitlab.com/ee/api/events.html).
The latter would allow me to distinguish by project, so I could do a mini-calendar for each project to track activity.
However, the projects page is statically generated, and API access requires a private token to be provided in the header of the request, so I couldn't use the API without exposing my access key.
Using calendar.json, while meaning the per-project information is lost, should work - but I ran into a CORS problem with fetch.
The GitLab server doesn't set the `Access-Control-Allow-Origin` header, so I can't fetch it from the browser (and GitLab apparently doesn't want me to, or at least they didn't consider it since it's a [semi-private](https://gitlab.com/gitlab-org/gitlab/-/issues/322153) endpoint in fairness).

This is an experimental repo to play with the Git calendar idea.
`glapi.go` looks at using the GitLab API.
`glcal.go` uses calendar.json.
Run them with `go run <filename.go>`.
My favourite solution, however, would be to simply grep the information from a directory of Git repositories on my own machine.
